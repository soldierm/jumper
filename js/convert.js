(function () {
    const location = 3
    const map = {
        "10.53.1.17": "2017code",
        "10.53.1.33": "未知仓库",
        "10.53.1.3": "mywayec",
        "10.53.1.4": "giftcard\rpdc",
        "10.53.1.5": "promotions-center",
        "10.53.1.6": "permission-center"
    }

    const th = '<th class="text-center"> CODE仓库 </th>'
    $("table#editable thead tr th").eq(location).after(th)

    $("table#editable tbody tr").each(function () {
        const position = $(this).children("td").eq(location)
        const key = $.trim(position.prev().text())
        let value = "未知仓库"
        if (map.hasOwnProperty(key)) {
            value = map[key]
        }
        const td = '<td class="text-center">' + value + '</td>'
        position.after(td)
    })
})()
